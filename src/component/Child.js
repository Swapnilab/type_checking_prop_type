import React, { Component } from 'react';
import ptr from 'prop-types';
//import { string } from 'yargs';

export default class Child extends Component {
  render() {
    return (
      <div>
        <h1>Hello {this.props.name} your roll no is {this.props.roll}</h1>
      </div>
    )
  }
}
Child.propTypes={
name:ptr.string.isRequired
};
Child.defaultProps={
name:"swapnil bhongale"
};
/*
 <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>

*/